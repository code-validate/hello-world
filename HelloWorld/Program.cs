﻿/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * Project          :   HelloWorld
 * Build Version    :   cv_hello_world_1.0
 * Platform         :   ASP.NET C# Console
 * Platform Version :   v4.5.2
 * Team             :   CodeValidate
 * Website          :   codevalidate.co.in
 * Contact          :   codevalidate@gmail.com
 * Description      :   To elaborate the purpose of the first program in ASP.NET C# and to demonstrate the execution of
 *                      the Hello, World program
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, World");
            Console.ReadKey();
        }
    }
}